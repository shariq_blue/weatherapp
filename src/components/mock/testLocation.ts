import { Location } from "../models/location";

export const testLocations: Location[] = [
  { locationName: "Asia/Karachi", postalCode: "74600" },
];
