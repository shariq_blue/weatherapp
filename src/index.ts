import { testLocations } from "./components/mock/testLocation";
import { getForecast } from "./components/forecast/forecast";

Promise.all(getForecast(testLocations))
  .then((result) => {
    result.forEach((data, index, arr) => {
      console.log(
        `Current Time : ${data.currentTime} | Weather : ${data.weather}`
      );
    });
  })
  .catch((e) => console.log(e));
